package fr.exatio.venumplugin;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;

import net.lightshard.prisonmines.MineAPI;
import net.lightshard.prisonmines.event.mine.MinePostResetEvent;
import net.lightshard.prisonmines.mine.Mine;

public class VenumListener implements Listener {
    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent event) {
        ItemStack itemStack;
        Player player;

        if ((event.getEntity().getKiller() != null) && ((player = event.getEntity().getKiller()).getInventory().getItemInHand() != null) && (itemStack = player.getInventory().getItemInHand()).hasItemMeta() && itemStack.getItemMeta().hasLore()) {
            StringBuilder builder = new StringBuilder("");

            for (String lore : itemStack.getItemMeta().getLore()) {
                builder.append(lore);
            }

            if (builder.toString().equals("Cette épée vous donne la tête du joueur que vous tuez")) {
                Player killer = event.getEntity().getKiller();
                ItemStack itemStack1 = new ItemStack(Material.SKULL_ITEM, 1, (short)SkullType.PLAYER.ordinal());
                SkullMeta itemMeta1 = (SkullMeta)itemStack1.getItemMeta();
                itemMeta1.setOwner(event.getEntity().getPlayer().getName());
                itemMeta1.setDisplayName(ChatColor.LIGHT_PURPLE + event.getEntity().getPlayer().getName());
                itemStack1.setItemMeta(itemMeta1);
                killer.getPlayer().getInventory().addItem(itemStack1);
            }
        }
    }

    @EventHandler
    public void onEntityDeathEvent(EntityDeathEvent event) {
        ItemStack itemStack;
        Player player;

        if ((event.getEntity().getKiller() != null) && ((player = event.getEntity().getKiller()).getInventory().getItemInHand() != null) && (itemStack = player.getInventory().getItemInHand()).hasItemMeta() && itemStack.getItemMeta().hasLore()) {
            StringBuilder builder = new StringBuilder("");

            for (String lore : itemStack.getItemMeta().getLore()) {
                builder.append(lore);
            }

            if (builder.toString().equals("Cette épée donne 2 fois plus d'xp lorsque vous tuez des mobs")) {
                event.setDroppedExp(event.getDroppedExp() * 2);
            }
        }
    }

    @EventHandler
    public void onMinePostReset(MinePostResetEvent event) {
        Mine mine = event.getMine();

        mine.getRegion().getPlayersInside().forEach(player -> {
            player.teleport(mine.getTeleportLocation());
            player.sendMessage("Reset : Vous avez été téléporté.");
        });
    }

    @EventHandler(priority=EventPriority.HIGH)
    public void onBlockBuilded(BlockPlaceEvent event) {
        if (Board.getInstance().getFactionAt(new FLocation(event.getBlock().getLocation())).isWarZone()) {
            MineAPI.PrisonMinesAPI minesAPI = new MineAPI.PrisonMinesAPI();

            for (Mine mine : minesAPI.getMines()) {
                if ((mine.getRegion() != null) && (mine.getRegion().getPlayersInside() != null) && mine.getRegion().getPlayersInside().contains(event.getPlayer())) {
                    event.setCancelled(false);
                }
            }
        }
    }

    @EventHandler(priority=EventPriority.HIGH)
    public void onBlockBreaked(BlockBreakEvent event) {
        if (Board.getInstance().getFactionAt(new FLocation(event.getBlock().getLocation())).isWarZone()) {
            MineAPI.PrisonMinesAPI minesAPI = new MineAPI.PrisonMinesAPI();

            for (Mine mine : minesAPI.getMines()) {
                if ((mine.getRegion() != null) && (mine.getRegion().getPlayersInside() != null) && mine.getRegion().getPlayersInside().contains(event.getPlayer())) {
                    event.setCancelled(false);
                }
            }
        }
    }
}
